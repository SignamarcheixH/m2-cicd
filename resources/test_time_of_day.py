import unittest

import time_of_day


class TestTimeOfDay(unittest.TestCase):


    def test_time_of_day(self):
        """
        Vérifie que la fonction fonctionne
        """
        self.assertEqual(time_of_day.get_time_of_day(12), "Bon apres midi")
        self.assertEqual(time_of_day.get_time_of_day(1), "Bonne nuit")
        self.assertEqual(time_of_day.get_time_of_day(20), "Bonne soiree")
        self.assertEqual(time_of_day.get_time_of_day(10), "Cafe du matin")


if __name__ == '__main__':
    unittest.main()