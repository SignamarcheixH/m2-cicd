def add(x,y):
    """
    Returns the addition of x and y
    Accepts both int and float and
    Returns an int or a float
    """
    return x+y

def substract(x,y):
    """
    Returns the substract of x and y
    Accepts both int and float and
    Returns an int or a float
    """
    return x-y

def multiply(x,y):
    """
    Returns the multiplication of x by y
    Accepts both int and float and
    Returns an int or a float
    """
    return x*y

def divide(x,y):
    """
    Returns the division of x by y
    Accepts both int and float and
    Returns an int or a float
    Raise a ValueError if y equals 0
    """
    if y == 0:
        raise ValueError('division par O impossible')
    else:
        return x/y