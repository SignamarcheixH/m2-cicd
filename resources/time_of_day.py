from datetime import datetime

def get_time_of_day(hour):
	"""
	Returns a string corresponding to the daytime given in parametet
	hour - The current hour ( 0 ~ 23 )
	"""

	if (hour >= 0 and hour < 6):
		return "Bonne nuit"
	if (hour >= 6 and hour < 12):
		return "Cafe du matin"
	if (hour >= 12 and hour < 18):
		return "Bon apres midi"

	return "Bonne soiree"


now = int(datetime.now().strftime("%H"))
print (get_time_of_day(now))