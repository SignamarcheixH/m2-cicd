import unittest

import mymath


class TestSum(unittest.TestCase):

    def test_add(self):
        """
        Verifie que l'addition fonctionne correctement
        et ce pour les differents types
        """
        self.assertEqual(mymath.add(1,2), 3)
        self.assertEqual(mymath.add(2,1), 3)
        self.assertEqual(mymath.add(5.7,2), 7.7)
        self.assertEqual(mymath.add(3.7,2.3), 6.0)


    def test_substract(self):
        """
        Verifie que la soustraction fonctionne correctement
        et ce pour les differents types
        """
        self.assertEqual(mymath.substract(2,1), 1)
        self.assertEqual(mymath.substract(1,2), -1)
        self.assertEqual(mymath.substract(5.7,2), 3.7)
        self.assertEqual(mymath.substract(5.7,2.0), 3.7)
        self.assertEqual(mymath.substract(0,2.3), -2.3)

    def test_multiply(self):
        """
        Verifie que la soustraction fonctionne correctement
        et ce pour les differents types
        """
        self.assertEqual(mymath.multiply(2,1), 2)
        self.assertEqual(mymath.multiply(20,12), 240)
        self.assertEqual(mymath.multiply(1,2), 2)
        self.assertEqual(mymath.multiply(0,2), 0)
        self.assertEqual(mymath.multiply(1,2.3), 2.3)
        self.assertEqual(mymath.multiply(1.0,2.3), 2.3)


    def test_divide(self):
        """
        Verifie que la division fonctionne
        """
        self.assertEqual(mymath.divide(4,2), 2)

        self.assertRaises(ValueError, mymath.divide, 4, 0)
        # une autre maniere d'ecrire cette instruction
        with self.assertRaises(ValueError):
            mymath.divide(4, 0)

if __name__ == '__main__':
    unittest.main()